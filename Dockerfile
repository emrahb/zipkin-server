FROM openjdk:8
ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/zipkin-server/zipkin-server.jar"]

ADD target/zipkin-server.jar /usr/share/zipkin-server/zipkin-server.jar